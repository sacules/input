package input

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
)

// Read from standard input until a newline is sent. It optionally
// takes a prompt that's printed before getting the text. In case
// more than one is provided, only the first is used.
func Read(prompt ...string) string {
	if len(prompt) > 0 {
		fmt.Print(prompt[0])
	}

	s := bufio.NewScanner(os.Stdin)
	s.Scan()
	return s.Text()
}

// ReadInt from standard input until a newline is sent. It optionally
// takes a prompt that's printed before getting the text. In case
// more than one is provided, only the first is used. Returns an error
// if the number couldn't be correctly parsed.
func ReadInt(prompt ...string) (int, error) {
	if len(prompt) > 0 {
		fmt.Print(prompt[0])
	}

	s := bufio.NewScanner(os.Stdin)
	s.Scan()
	text := s.Text()

	var num int
	num, err := strconv.Atoi(text)
	if err != nil {
		return num, fmt.Errorf("converting '%s' to int: %v", text, err)
	}

	return num, nil
}
